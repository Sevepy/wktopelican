#!/usr/bin/env bash
rm ./Documents/*.hdr
rm ./Documents/journal/*.hdr
rm ./content/*.html
rm ./content/*.ipynb
rm ./content/*.nbdata
rm ./public/rss.xml
rm ./public/atom.xml
#rm -rf ./public/*
rm ./tags/*
