#!/usr/bin/env bash
pandoc --template pandoc-template.html -H ./Documents/About_Me.wiki.html.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/About_Me.html.html -s ./Documents/About_Me.wiki.html.wiki
pandoc --template pandoc-template.html -H ./Documents/Blogroll.wiki.html.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/Blogroll.html.html -s ./Documents/Blogroll.wiki.html.wiki
pandoc --template pandoc-template.html -H ./Documents/index.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/index.html -s ./Documents/index.wiki
pandoc --template pandoc-template.html -H ./Documents/My_Bookmarks.wiki.html.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/My_Bookmarks.html.html -s ./Documents/My_Bookmarks.wiki.html.wiki
pandoc --template pandoc-template.html -H ./Documents/Read_more_about_my_Windows_apps.wiki.html.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/Read_more_about_my_Windows_apps.html.html -s ./Documents/Read_more_about_my_Windows_apps.wiki.html.wiki
pandoc --template pandoc-template.html -H ./Documents/SeveTech_Deep_Dive_Discussions.wiki.html.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/SeveTech_Deep_Dive_Discussions.html.html -s ./Documents/SeveTech_Deep_Dive_Discussions.wiki.html.wiki
pandoc --template pandoc-template.html -H ./Documents/Static_Website_Mechanics.wiki.html.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/Static_Website_Mechanics.html.html -s ./Documents/Static_Website_Mechanics.wiki.html.wiki
pandoc --template pandoc-template.html -H ./Documents/TEMU_MUST_HAVE.wiki.html.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/TEMU_MUST_HAVE.html.html -s ./Documents/TEMU_MUST_HAVE.wiki.html.wiki
pandoc --template pandoc-template.html -H ./Documents/Unique_Notebooks.wiki.html.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/Unique_Notebooks.html.html -s ./Documents/Unique_Notebooks.wiki.html.wiki
pandoc --template pandoc-template.html -H ./Documents/VIM.wiki.html.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/VIM.html.html -s ./Documents/VIM.wiki.html.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2020-12-11.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2020-12-11.html -s ./Documents/journal/2020-12-11.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2020-12-15.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2020-12-15.html -s ./Documents/journal/2020-12-15.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2020-12-21.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2020-12-21.html -s ./Documents/journal/2020-12-21.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2021-01-10.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2021-01-10.html -s ./Documents/journal/2021-01-10.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2021-01-14.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2021-01-14.html -s ./Documents/journal/2021-01-14.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2021-01-25.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2021-01-25.html -s ./Documents/journal/2021-01-25.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2021-01-29.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2021-01-29.html -s ./Documents/journal/2021-01-29.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2021-02-04.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2021-02-04.html -s ./Documents/journal/2021-02-04.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2021-02-10.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2021-02-10.html -s ./Documents/journal/2021-02-10.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2021-02-18.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2021-02-18.html -s ./Documents/journal/2021-02-18.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2021-02-21.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2021-02-21.html -s ./Documents/journal/2021-02-21.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2021-03-04.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2021-03-04.html -s ./Documents/journal/2021-03-04.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2021-03-14.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2021-03-14.html -s ./Documents/journal/2021-03-14.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2021-03-21.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2021-03-21.html -s ./Documents/journal/2021-03-21.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2021-04-21.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2021-04-21.html -s ./Documents/journal/2021-04-21.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2021-04-29.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2021-04-29.html -s ./Documents/journal/2021-04-29.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2021-05-10.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2021-05-10.html -s ./Documents/journal/2021-05-10.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2021-05-27.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2021-05-27.html -s ./Documents/journal/2021-05-27.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2021-06-16.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2021-06-16.html -s ./Documents/journal/2021-06-16.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2021-06-28.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2021-06-28.html -s ./Documents/journal/2021-06-28.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2021-07-14.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2021-07-14.html -s ./Documents/journal/2021-07-14.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2021-07-27.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2021-07-27.html -s ./Documents/journal/2021-07-27.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2021-08-12.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2021-08-12.html -s ./Documents/journal/2021-08-12.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2021-08-21.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2021-08-21.html -s ./Documents/journal/2021-08-21.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2021-08-26.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2021-08-26.html -s ./Documents/journal/2021-08-26.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2021-09-06.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2021-09-06.html -s ./Documents/journal/2021-09-06.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2021-09-15.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2021-09-15.html -s ./Documents/journal/2021-09-15.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2021-09-18.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2021-09-18.html -s ./Documents/journal/2021-09-18.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2021-09-25.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2021-09-25.html -s ./Documents/journal/2021-09-25.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2021-09-28.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2021-09-28.html -s ./Documents/journal/2021-09-28.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2021-10-03.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2021-10-03.html -s ./Documents/journal/2021-10-03.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2021-10-11.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2021-10-11.html -s ./Documents/journal/2021-10-11.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2021-10-24.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2021-10-24.html -s ./Documents/journal/2021-10-24.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2021-11-03.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2021-11-03.html -s ./Documents/journal/2021-11-03.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2021-11-20.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2021-11-20.html -s ./Documents/journal/2021-11-20.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2021-11-24.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2021-11-24.html -s ./Documents/journal/2021-11-24.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2021-12-03.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2021-12-03.html -s ./Documents/journal/2021-12-03.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2021-12-07.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2021-12-07.html -s ./Documents/journal/2021-12-07.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2021-12-14.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2021-12-14.html -s ./Documents/journal/2021-12-14.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2021-12-25.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2021-12-25.html -s ./Documents/journal/2021-12-25.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2022-01-04.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2022-01-04.html -s ./Documents/journal/2022-01-04.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2022-01-10.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2022-01-10.html -s ./Documents/journal/2022-01-10.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2022-01-19.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2022-01-19.html -s ./Documents/journal/2022-01-19.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2022-01-23.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2022-01-23.html -s ./Documents/journal/2022-01-23.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2022-02-04.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2022-02-04.html -s ./Documents/journal/2022-02-04.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2022-02-10.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2022-02-10.html -s ./Documents/journal/2022-02-10.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2022-02-23.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2022-02-23.html -s ./Documents/journal/2022-02-23.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2022-03-03.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2022-03-03.html -s ./Documents/journal/2022-03-03.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2022-03-08.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2022-03-08.html -s ./Documents/journal/2022-03-08.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2022-03-13.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2022-03-13.html -s ./Documents/journal/2022-03-13.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2022-03-23.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2022-03-23.html -s ./Documents/journal/2022-03-23.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2022-03-27.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2022-03-27.html -s ./Documents/journal/2022-03-27.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2022-04-03.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2022-04-03.html -s ./Documents/journal/2022-04-03.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2022-04-11.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2022-04-11.html -s ./Documents/journal/2022-04-11.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2022-04-14.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2022-04-14.html -s ./Documents/journal/2022-04-14.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2022-04-22.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2022-04-22.html -s ./Documents/journal/2022-04-22.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2022-04-27.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2022-04-27.html -s ./Documents/journal/2022-04-27.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2022-05-03.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2022-05-03.html -s ./Documents/journal/2022-05-03.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2022-05-12.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2022-05-12.html -s ./Documents/journal/2022-05-12.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2022-05-16.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2022-05-16.html -s ./Documents/journal/2022-05-16.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2022-05-22.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2022-05-22.html -s ./Documents/journal/2022-05-22.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2022-05-28.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2022-05-28.html -s ./Documents/journal/2022-05-28.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2022-06-09.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2022-06-09.html -s ./Documents/journal/2022-06-09.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2022-06-13.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2022-06-13.html -s ./Documents/journal/2022-06-13.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2022-06-17.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2022-06-17.html -s ./Documents/journal/2022-06-17.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2022-06-22.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2022-06-22.html -s ./Documents/journal/2022-06-22.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2022-06-26.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2022-06-26.html -s ./Documents/journal/2022-06-26.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2022-07-01.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2022-07-01.html -s ./Documents/journal/2022-07-01.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2022-07-03.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2022-07-03.html -s ./Documents/journal/2022-07-03.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2022-07-11.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2022-07-11.html -s ./Documents/journal/2022-07-11.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2022-07-17.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2022-07-17.html -s ./Documents/journal/2022-07-17.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2022-07-21.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2022-07-21.html -s ./Documents/journal/2022-07-21.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2022-07-26.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2022-07-26.html -s ./Documents/journal/2022-07-26.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2022-08-05.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2022-08-05.html -s ./Documents/journal/2022-08-05.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2022-08-18.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2022-08-18.html -s ./Documents/journal/2022-08-18.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2022-08-22.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2022-08-22.html -s ./Documents/journal/2022-08-22.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2022-08-26.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2022-08-26.html -s ./Documents/journal/2022-08-26.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2022-09-16.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2022-09-16.html -s ./Documents/journal/2022-09-16.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2022-10-10.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2022-10-10.html -s ./Documents/journal/2022-10-10.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2022-10-18.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2022-10-18.html -s ./Documents/journal/2022-10-18.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2022-10-31.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2022-10-31.html -s ./Documents/journal/2022-10-31.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2022-11-19.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2022-11-19.html -s ./Documents/journal/2022-11-19.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2022-11-25.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2022-11-25.html -s ./Documents/journal/2022-11-25.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2022-12-08.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2022-12-08.html -s ./Documents/journal/2022-12-08.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2022-12-16.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2022-12-16.html -s ./Documents/journal/2022-12-16.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2022-12-28.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2022-12-28.html -s ./Documents/journal/2022-12-28.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2023-01-06.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2023-01-06.html -s ./Documents/journal/2023-01-06.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2023-02-06.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2023-02-06.html -s ./Documents/journal/2023-02-06.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2023-02-20.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2023-02-20.html -s ./Documents/journal/2023-02-20.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2023-03-13.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2023-03-13.html -s ./Documents/journal/2023-03-13.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2023-03-17.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2023-03-17.html -s ./Documents/journal/2023-03-17.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2023-03-30.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2023-03-30.html -s ./Documents/journal/2023-03-30.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2023-04-14.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2023-04-14.html -s ./Documents/journal/2023-04-14.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2023-04-25.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2023-04-25.html -s ./Documents/journal/2023-04-25.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2023-04-29.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2023-04-29.html -s ./Documents/journal/2023-04-29.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2023-05-09.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2023-05-09.html -s ./Documents/journal/2023-05-09.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2023-05-22.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2023-05-22.html -s ./Documents/journal/2023-05-22.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2023-06-06.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2023-06-06.html -s ./Documents/journal/2023-06-06.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2023-06-22.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2023-06-22.html -s ./Documents/journal/2023-06-22.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2023-07-07.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2023-07-07.html -s ./Documents/journal/2023-07-07.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2023-07-22.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2023-07-22.html -s ./Documents/journal/2023-07-22.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2023-08-19.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2023-08-19.html -s ./Documents/journal/2023-08-19.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2023-08-23.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2023-08-23.html -s ./Documents/journal/2023-08-23.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2023-08-29.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2023-08-29.html -s ./Documents/journal/2023-08-29.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2023-09-21.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2023-09-21.html -s ./Documents/journal/2023-09-21.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2023-10-31.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2023-10-31.html -s ./Documents/journal/2023-10-31.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2023-11-15.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2023-11-15.html -s ./Documents/journal/2023-11-15.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2023-11-27.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2023-11-27.html -s ./Documents/journal/2023-11-27.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2023-12-11.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2023-12-11.html -s ./Documents/journal/2023-12-11.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2023-12-23.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2023-12-23.html -s ./Documents/journal/2023-12-23.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2024-01-06.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2024-01-06.html -s ./Documents/journal/2024-01-06.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2024-01-11.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2024-01-11.html -s ./Documents/journal/2024-01-11.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2024-01-25.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2024-01-25.html -s ./Documents/journal/2024-01-25.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2024-02-20.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2024-02-20.html -s ./Documents/journal/2024-02-20.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2024-03-01.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2024-03-01.html -s ./Documents/journal/2024-03-01.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2024-03-22.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2024-03-22.html -s ./Documents/journal/2024-03-22.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2024-03-30.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2024-03-30.html -s ./Documents/journal/2024-03-30.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2024-04-16.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2024-04-16.html -s ./Documents/journal/2024-04-16.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2024-04-29.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2024-04-29.html -s ./Documents/journal/2024-04-29.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2024-05-07.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2024-05-07.html -s ./Documents/journal/2024-05-07.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2024-05-22.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2024-05-22.html -s ./Documents/journal/2024-05-22.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2024-06-05.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2024-06-05.html -s ./Documents/journal/2024-06-05.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2024-06-09.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2024-06-09.html -s ./Documents/journal/2024-06-09.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2024-06-27.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2024-06-27.html -s ./Documents/journal/2024-06-27.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2024-07-01.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2024-07-01.html -s ./Documents/journal/2024-07-01.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2024-07-09.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2024-07-09.html -s ./Documents/journal/2024-07-09.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2024-07-18.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2024-07-18.html -s ./Documents/journal/2024-07-18.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2024-07-31.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2024-07-31.html -s ./Documents/journal/2024-07-31.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2024-08-13.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2024-08-13.html -s ./Documents/journal/2024-08-13.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2024-08-30.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2024-08-30.html -s ./Documents/journal/2024-08-30.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2024-09-05.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2024-09-05.html -s ./Documents/journal/2024-09-05.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2024-09-28.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2024-09-28.html -s ./Documents/journal/2024-09-28.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2024-10-13.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2024-10-13.html -s ./Documents/journal/2024-10-13.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2024-10-19.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2024-10-19.html -s ./Documents/journal/2024-10-19.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2024-11-29.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2024-11-29.html -s ./Documents/journal/2024-11-29.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2024-12-10.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2024-12-10.html -s ./Documents/journal/2024-12-10.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2024-12-23.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2024-12-23.html -s ./Documents/journal/2024-12-23.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2025-01-14.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2025-01-14.html -s ./Documents/journal/2025-01-14.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2025-01-21.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2025-01-21.html -s ./Documents/journal/2025-01-21.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2025-02-06.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2025-02-06.html -s ./Documents/journal/2025-02-06.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2025-02-20.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2025-02-20.html -s ./Documents/journal/2025-02-20.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2025-03-03.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2025-03-03.html -s ./Documents/journal/2025-03-03.wiki
~/workspace/toolbox/apps/node/node_modules/.bin/bs export --web Documents/presentations/'Remarkpy Example.md' -o public/presentations/Remarkpy_Example/
