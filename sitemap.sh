#!/bin/bash

SITEMAP="./public/sitemap.xml"
BASEURL="https://tessarinseve.pythonanywhere.com/nws"

if ! grep -q "$BASEURL"  "$SITEMAP"; then

    sed -i 's#<loc>/#<loc>https://tessarinseve.pythonanywhere.com/nws/#g' "$SITEMAP"
    sed -i 's/\.wiki\.html\.wiki\.html/\.wiki\.html/g' "$SITEMAP"
fi
cp ./google2924a9a6e517ea50.html ./public/
cp ./pinterest-bea95.html ./public/
cp ./robots.txt ./public/
