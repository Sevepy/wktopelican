== Personal Knowledge Management With VIM and Logseq ==


'''''Personal Knowledge Management''''' (PKM) systems have emerged as powerful tools for individuals to collect, organize, and retrieve information effectively. At its core, a PKM system serves as a centralized hub for storing personal insights, notes, ideas, and resources, enabling users to cultivate a repository of knowledge tailored to their unique needs and interests.

=== Logseq ===

[https://logseq.com Logseq] is a versatile tool that can be effectively utilized for journaling, an important part of a PKM. An empty Logseq graph is a folder with a predefined structure, shown below.
<pre>
    +-- journals
    +-- logseq
    |   +-- .recycle
    |   +-- config.edn
    |   +-- custom.css
    +-- pages
    |   +-- contents.md
</pre>
Each day's entry will be stored in a text file within the ''journals'' directory.
The setting for the preferred date format is shown in the figure below.

Image("logseq_setting.PNG")


=== Wiki.vim ===

The Logseq graph filesystem structure closely aligns with the organization created by the Vim plugin [https://github.com/lervag/wiki.vim  wiki.vim], especially when configured as follows:
<pre>
    " wiki.vim customization
    let g:wiki_root = expand("%:p:h") 
    let g:wiki_filetypes=["md"]
    let g:wiki_link_target_type='md'
    let g:wiki_mappings_use_defaults='all'
    let g:wiki_mappings_local = {
                \ '<plug>(wiki-journal-prev)' : '<c-h>',
                \ '<plug>(wiki-journal-next)' : '<c-l>',
                \}                                                                                 
    call wiki#init#option('wiki_index_name', 'contents')
    call wiki#init#option('wiki_journal', {
          \ 'name' : 'journals',
          \ 'root' : '',
          \ 'frequency' : 'daily',
          \ 'date_format' : {
          \   'daily' : '%Y_%m_%d',
          \   'weekly' : '%Y_w%V',
          \   'monthly' : '%Y_m%m',
          \ },
          \})
</pre>

=== LSP === 

LSP can be implemented with the [https://github.com/yegappan/lsp Language Server Protocol (LSP) plugin] utilizing the [https://github.com/Feel-ix-343/markdown-oxide Markdown Language Server] for enhanced markdown editing capabilities.


TEMU('indexcard')

The configuration for the LSP can included in the root directory of the graph with the following .moxide.toml file:
<pre>
    dailynote = "%Y_%m_%d" 
    heading_completions = true
    title_headings = false
    unresolved_diagnostics = false
    semantic_tokens = false
    tags_in_codeblocks = true
    references_in_codeblocks = true
    new_file_folder_path = "C:\\Users\\Seve\\workspace\\GridMemo\\pages"
    daily_notes_folder = "C:\\Users\\Seve\\workspace\\GridMemo\\journals"
    include_md_extension_md_link = false
    include_md_extension_wikilink = false
    hover = false
</pre>


=== Calendar and Fuzzy File Finder ===

The last segment of the video below demonstrates the integration of [https://tessarinseve.pythonanywhere.com/nws/2021-11-24.wiki.html Vim's plugin calendar] and the [https://tessarinseve.pythonanywhere.com/nws/2024-04-29.wiki.html fuzzy file finder], enhancing navigation within the graph.

The fuzzy file finder conveniently hides supporting files and directories using the following ''.fdignore'' file.


<pre>
    logseq
    vimrc.local
</pre>

YouTubeVideo("IquOrDi3zJg")
