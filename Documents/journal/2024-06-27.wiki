
== A Simple Build System ==

The [https://ninja-build.org Ninja build system] is a fast and efficient build tool commonly used in conjunction with build configuration systems like [https://mesonbuild.com/index.html Meson] to compile software projects. Ninja is known for its speed and scalability, making it a popular choice for large-scale projects requiring quick build times.

On Windows, it is possible to install Ninja via the MSYS2 subsystem using the following command:
    pacman -S ninja


The video below showcases an example of Vlang source code. When compiled, it generates an executable that functions as a terminal pager for text files.

The Ninja build file can be generated from a Python script, as demonstrated in the initial segment of the video.

YouTubeVideo("epWY0jv2tW4")


=== Integration with VIM ===

This workflow incorporates the [https://github.com/tandrewnichols/vim-determined vim-determined] plugin, which provides a function for creating commands that can be executed asynchronously in the terminal.

To initiate the build process, the command ''Ninja'' can be executed from VIM by defining this custom command:

    call determined#command('Ninja', 'ninja', { 'autoclose': 0, 'reuse': 1 })   


=== Dot Output ===

The Ninja build system can generate a graphical representation of the build process in the dot format with the following option:
     ninja -t graph


=== ''build.ninja'' Generation Using a Python Script ===

The following Python script uses the ''ninja_syntax.py'' module available in the Ninja distribution.

<pre>
    from ninja_syntax import ninja_syntax as ns
    from io import StringIO

    w = StringIO()
    ninja = ns.Writer(w)
    ninja.rule(name='v_compile', description='Compiling $in', command='v -o $out $in', depfile='$in')
    ninja.build('pager.exe', 'v_compile', 'pager.v')
    w.seek(0)
    print("".join(w.readlines()))

</pre>

It creates the following ''build.ninja'':

<pre>
rule v_compile
  command = v -o $out $in
  description = Compiling $in
  depfile = $in
build pager.exe: v_compile pager.v
</pre>
