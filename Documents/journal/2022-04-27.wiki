== Extending Bootstrap Single Page Layout with Flaskerizer and Flask Frozen ==

Bootstrap is my favorite CSS framework, some of [https://bootstrap.build/templates the best free examples] are just single page designs. Here I going to be showing how I add extra pages to an existing Bootstrap website.

[https://pypi.org/project/flaskerizer/ Flasekrizer] (pip install flaskerizer) creates a Flask application out of a Bootstrap template. A typical command line usage for a co-localized Bootstrap template and index.html file:

    $ flaskerizer -i ./service -t ./service -o ./ -S -n service_extended

will produce a Flask application inside, in this case, the directory ''service_extended''.

I can then copy and rename index.html to base.html. Generally, I can remove from base.html everything in between the closing ''nav'' tag and the opening ''footer'' tag, then add the placeholder :

    <pre>
    {% block content %}
    {% endblock %}
    </pre>

For index.html I can essentially do the opposite, keep what I have removed in base.html and enclose it inside a Jinja2 block:
    
    <pre>
    {% extends 'base.html'}
    {% block content %}
    ...
    {% endblock %}
    </pre>

A new page will again extend the base.html file.

Finally, I just need to add the new endpoints and ''freeze'' the Flask app into a set of static files with [https://pypi.org/project/Frozen-Flask/ Frozen-Flask] (pip install Frozen-Flask), ''build'' is the default destination folder.

    <pre>

    from flask import Flask, render_template
    from flask_frozen import Freezer
    app = Flask(__name__)
    freezer = Freezer(app)


    @app.route('/index.html')
    @app.route('/')
    def index():
        return render_template('index.html',title = 'Index')

    @app.route('/about.html')
    def about():
        return render_template('about.html',title = 'About Us')

    if __name__ == '__main__':
        freezer.freeze()

    </pre>



Image("tree_flask_bootstrap.PNG")
