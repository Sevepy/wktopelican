== Replicating VOX Features in PowerShell: A Python Virtual Environment Management Script ==
 
=== Introduction ===
I find myself frequently juggling multiple python projects, each with its own set of dependencies. Managing virtual environments is crucial for isolating these dependencies and maintaining a clean development environment. While tools like ''virtualenv'' is a popular choice, I decided to create a custom PowerShell script that replicates some of the features found in the [https://github.com/xonsh/xontrib-vox vox] tool.

=== What's vox? ===

[https://github.com/xonsh/xontrib-vox Vox] is primarily associated with the [https://tessarinseve.pythonanywhere.com/nws/2024-01-25.wiki.html Xonsh shell (a Python-powered shell)], and it provides a convenient command-line interface for creating, activating, and managing Python virtual environments. While it's similar to virtualenv, vox aims to be more user-friendly and intuitive, making it a favorite among Xonsh users.

=== The Motivation Behind My Custom Script ===

As a PowerShell enthusiast, I wanted a [https://buymeacoffee.com/seve/replicating-vox-features-powershell-a-python-virtual-environment-management-script native solution for managing virtual environments]. Here's why I decided to create my own script:

''PowerShell Familiarity'': I am comfortable with PowerShell, and it's my go-to shell for various tasks. Leveraging PowerShell allows me to stay within my preferred environment.

''Customization and Flexibility'': By writing my own script, I have full control over its behavior.


== PDF FreeText TODO ==

PDFs are a ubiquitous format for sharing documents, reports, and forms. In Python, we have powerful libraries that allow us to create, edit, and modify PDF files effortlessly like PyPDF, FPDF2, [https://github.com/pymupdf PYMUPDF], and Borb PDF.

However, it’s essential to be aware of licensing restrictions, especially when considering commercial use. Many libraries impose limitations that require purchasing a license for commercial applications.

[https://pypi.org/project/pypdf/ PyPDF] stands out because it is released under a permissive license that allows commercial use.  

=== Setting Up a Dedicated Virtual Environment for PDF Editing ===

Before we dive into PDF editing, let’s ensure we’re working in an isolated environment specifically tailored for this task. We’ll create a virtual environment named ''pdf_env'' using the PowerShell script vox.ps1 as shown in the video below.

YouTubeVideo("_tAW7q_isLQ")

Now that we have our dedicated virtual environment set up, let’s install the library PyPDF with:
    pip install pypdf

A FreeText annotation is a type of markup that allows you to add text directly onto a PDF page. It's like placing a sticky note or comment within the document.
Unlike regular text content in a PDF, FreeText annotations are separate objects that float above the page content. They can be positioned anywhere on the page.

When you create a FreeText annotation as shown in the video above, it doesn’t directly embed the text as part of the page content.

Different PDF viewers (such as Adobe Reader, Google Chrome, or mobile apps) may interpret and render FreeText differently. The rendering quality (anti-aliasing, scaling, etc.) can also vary. Because of these variations, the appearance of FreeText annotations can differ significantly across different PDF viewers.


The vox.ps1 PowerShell script and the Python cli script for FreeText annotations used in the video can be downloaded [https://buymeacoffee.com/seve/replicating-vox-features-powershell-a-python-virtual-environment-management-script here]


=== Command Line Tool ===

[https://github.com/py-pdf/pdfly Pdfly] (PDF-li) is a pure-python cli application based on Pypdf library.
