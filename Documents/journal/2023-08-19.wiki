

== How Pareto Charts Help to Make Informed Decisions ==

This entry describes how I created the presentation below with [https://www.bing.com the Bing search engine's] generative large language model (LLM).


YouTubeVideo("pceHSc6mPJ0")

After navigating to Bing's search Copilot, I typed inside the prompt the following request:

    As a senior project manager, you are tasked to draft a four slide presentation using markdown markups on how Pareto charts help to make informed decisions.
    The title slide should have at the top the following line: 
    class: centre, middle
    Please provide me with the markdown raw text for the four slides.

The Copilot's output can then be copied to a text editor and saved it locally as a markdown text file. Tailwind's classes can also be added to either the entire slide or single elements. 

Finally, I used [https://www.buymeacoffee.com/seve/remarkpy-windows-markdown-presentation-tool Remarkpy] with the [https://tessarinseve.pythonanywhere.com/nws/2022-05-16.wiki.html tailwind theme] selected to open the below markdown formatted text on the default browser.


<pre>

    class: center, middle, bg-[url('static/files/pareto2.jpg')], text-zinc-200
    # How Pareto Charts Help to Make Informed Decisions

    ---
    class: top,left , bg-[url('static/files/pareto.jpg')], text-zinc-600, bg-cover, font-bold, text-2xl, outline-4
    .bg-gray-200[
    .p-6[
    ## What is a Pareto Chart?
    ]]


    .p-6[
    .bg-gray-300[
    A type of chart that contains both bars and a line graph. The bars represent individual values in descending order, the line represents the cumulative total or percentage of the values.
    The chart is named for the Pareto principle, which states that 80% of the effects come from 20% of the causes.
    ]]

    ---
    class: middle,left , bg-[url('static/files/pareto.jpg')], text-zinc-600, bg-cover, font-bold, text-2xl, outline-4, list-decimal
    .bg-gray-200[
    .p-6[
    ## When to Use a Pareto Chart?
    ]]
    .bg-gray-300[
    .p-3[
    - When analyzing data about the frequency of problems or causes in a process
    - When there are many problems or causes and you want to focus on the most significant ones
    - When analyzing broad causes by looking at their specific components
    - When communicating with others about your data.
    ]]
    ---
    class: middle,left , bg-[url('static/files/pareto.jpg')], text-zinc-600, bg-cover, font-bold, text-2xl, outline-4
    .bg-gray-200[
    .p-6[
    ## How to Create a Pareto Chart?
    ]]
    .bg-gray-300[
    .p-3[
    - Decide what categories you will use to group items
    - Decide what measurement is appropriate (e.g., frequency, cost, time)
    - Choose a timeframe for collecting the data
    - For each category, tally how often it occurred (or cost or total time)
    - Subtotal the measurements for each category
    - Determine the appropriate scale for the measurements
    - Draw and label the left vertical axis for the measurements
    - Draw and label the right vertical axis for the cumulative percentage
    - Draw and label the horizontal axis for the categories
    - Draw the bars for each category in descending order of measurement
    - Calculate and draw the line for the cumulative percentage
    ]]

</pre>


