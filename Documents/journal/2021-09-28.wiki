
== Maxima CAS REPL history to PDF ==

Maxima computer algebra system (CAS) is a rather old program, nevertheless, it's still probably the best free CAS available.
I have never enjoyed using the UI version, and on Windows, the REPL opens inside the Command Prompt, not really the maximum in terms of user experience.

With the new Windows Terminal (WT), however, you can now enjoy Maxima REPL in a more modern, fast and really productive terminal environment.

=== Create a new Windows Terminal profile ===

Getting Maxima REPL in one of WT tabs is really straightforward.

Open the Windows Terminal ''Settings'', with <kdb>CTRL+,</kdb> hotkey and create a new ''Profile'' as shown in the figure below.

Image('maximaCASprofile.png')

In this case, I have Maxima installed in the default location (C:/maxima-5.42.2). This path may be different and depends on where, in your system, Maxima's executables have been installed.

=== Maxima configuration files ===

The first time you open the REPL, Maxima creates its own configuration directory, the default location is '''''%USERPROFILE%/maxima'''''. Inside this directory, you can find ''maxima-init.mac'', which contains variables and commands that have to be executed at the startup. I modified it as shown below, it loads the ''alt-display'' module and sets the output for ''TeX'' compatibility [1]. 
It also opens a new file (maxout.md) that records the transcript of the current session [2].

    <pre>
    /* last edit: 7-28-09 */
    maxima_userdir: "~/maxima" $
    maxima_tempdir: "~/maxima" $
    load("alt-display.mac")$

    set_alt_display(2, lambda([form], tex(?caddr(form))))$ /*[1]*/
    gnuplot_command: "winpty /c/gnuplot/bin/gnuplot.exe"$ 
    set_plot_options([gnuplot_term, png])$
    set_draw_defaults(terminal=png)$
    set_plot_option([plot_format,gnuplot])$
    set_plot_option([run_viewer,false])$

    stardisp:true;
    disp("Maxima CAS")$
    system("del %USERPROFILE%\\Desktop\\maxout.md")$ 
    writefile("~/Desktop/maxout.md")$ /*[2]*/
    </pre>

=== Generate PDF with RemarkPy ===

The video below shows how to select the theme that allows displaying Maxima's transcript file in the browser. Then, you just need to print the HTML page to get a PDF version of your latest Maxima session.

YouTubeVideo("MhGDtIMIt00")
