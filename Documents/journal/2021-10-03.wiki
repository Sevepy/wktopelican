
== Browser automation on Windows - Mingw64/Msys2 ==

I have recently discovered [https://github.com/Rasukarusan/shellnium Shellium], a really interesting project that brings the power of browser automation to the bash shell. Out of pure curiosity, I am going to explore if it also works with ''Edge'' the pre-installed browser in Windows 10.

First, I should get the right ''webdriver'', a ZIP file that contains the executable for Edge's stable channel can be downloaded [https://developer.microsoft.com/en-us/microsoft-edge/tools/webdriver/ here] and then unpacked on a local directory. From the Mingw64 shell I can start the webdriver as a background process with:
    $ edgedriver &

'''''Shellium''''' depends on the command ''jq'' (JSON-processor), among the following options available for a ''MSYS2/MINGW'' installation, the one compiled with CLANG is the most preferable in my opinion.

    <pre>
    mingw32/mingw-w64-i686-jq 1.6-4
        Command-line JSON processor (mingw-w64)
    mingw64/mingw-w64-x86_64-jq 1.6-4
        Command-line JSON processor (mingw-w64)
    ucrt64/mingw-w64-ucrt-x86_64-jq 1.6-4
        Command-line JSON processor (mingw-w64)
    clang64/mingw-w64-clang-x86_64-jq 1.6-4 [installed]
        Command-line JSON processor (mingw-w64)
    </pre>

To Install the executable compiled with clang64 I can use the Mingw64 package manager as follows:

    $ pacman -S mingw-w64-clang-x86_64-jq

and then include the clang binaries to the shell PATH:

    export PATH="/c/msys64/clang64/bin:${PATH}"

So far, so good. Now, I need to get '''''Shellium''''' from the following repository:
    
    $ git clone https://github.com/Rasukarusan/shellnium 

This simple bash script opens my dictionary web application, passes a test word to the web-form, clicks the submit button, and collects the word's definition displayed on the web page. Finally, it prints the word's definition to the standard output.

    <pre>
    #!/usr/bin/env bash
    #filenme: mydictionary,sh
    source ./lib/selenium.sh --headless

    main() {
        if [ "$(is_ready)" != 'true' ]; then
            printf "\e[35m[ERROR] driver is not running.\e[m\n"
            exit
        fi

        navigate_to 'https://tessarinseve.pythonanywhere.com/dictionary'

        local word=$(find_element 'name' 'word')
        send_keys $word $1

        local button=$(find_elements 'name' 'singlebutton')
        click ${button}
        
        local definition=$(find_elements 'id' 'log')
        get_text ${definition}
        delete_session
    }



    main $1
    </pre>

To run the previous bash script, I just have to type :

    $ ,/mydictionary.sh now

where ''now'' is the word that I am looking up the meaning of. In VIM, I can redirect the script's output to the working buffer with '':r! ./mydictionary.sh now''.

YouTubeVideo("TcMU5PAZp8o")




