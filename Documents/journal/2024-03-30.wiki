
== Vim9Script Plugin: Typst-Powershell ==


[https://tessarinseve.pythonanywhere.com/nws/2023-04-29.wiki.html Typst], a markup-based typesetting system, provides built-in markup for common formatting tasks. 

It integrates a scripting system that lets you input code directly into your documents which is especially useful for creating tables, or defining custom elements.

To use Typst from the command line in a PowerShell terminal, you have to invoke the Typst command-line interface. This CLI allows for compiling Typst documents locally. 

The video below demonstrates a [https://github.com/sevehub/typstpowershell Vim] plugin designed to simplify the compilation of Typst source code into PDF files. 

Errors are displayed in a pop-up window. You can also easily recall the PDF viewer (:PDFViewer), and pin it to your desktop with the keyboard shortcut CTRL-SHIFT-SPACE (Windows OS only) for a seamless workflow.

YouTubeVideo("9wYYGdtSrBw")


A Vim command (:TypstFonts) has also been added to display the fonts available in the system within a separate dedicated buffer.


=== Configuration ===

This Vim plugin provides the flexibility to define different PDF viewers for displaying compiled documents. Specifically, you can configure it to use [https://www.sumatrapdfreader.org/free-pdf-reader SumatraPDF] as your preferred PDF viewer.

To set a PDF viewer add one of the following lines to your .vimrc file:
<pre>
    let g:pdf_viewer = 'SumatraPDF.exe' ""legacy
    g:pdf_viewer = 'SumatraPDF.exe' #vim9script
</pre>

If the executable is not in a directory included in the PATH variable, the full path is required.
