
== Typst Typesetting System ==


[https://github.com/typst/typst Typst] is a new markup-based typesetting system with considerable advantages over existing ones.

Typst compiler is a single command line pre-built executable, available also for Windows OS, which is only tens of megabytes large. It supports a set of built-in markups for the most common formatting elements, and it integrates a simple, yet powerful, scripting language inside the document.

The video below demonstrates how to get started with Typst on Windows OS, while editing documents with VIM text editor. 

YouTubeVideo("T9QW8BUxbOE")

Open a typst source file in Vim and start ( to use the ''Start'' or ''Start!'' command as shown in the video, install [https://tessarinseve.pythonanywhere.com/nws/2022-01-19.wiki.html Dispatch.vim plugin]) the typst compiler in ''watch mode'' which automatically recompiles the active source file into a PDF.

From a [https://tessarinseve.pythonanywhere.com/nws/2023-02-20.wiki.html Vim terminal window], open the compiled PDF file with ''Evince'', a PDF viewer. Evince (Mingw-w64) can be installed with:
    pacman -S evince

Edit the source code and save it, the updated PDF document will be automatically shown inside the viewer.


=== typst.vim plugin ===

[https://github.com/kaarmu/typst.vim Typst.vim plugin] provides syntax highlighting for the ''typst'' filetype which is automatically set in Vim for files with the .typ suffix.

It has also a filetype-compiler which can compile the active buffer with the command '':make'' and show eventual errors in a quickfix list. Alternatively, you can source the following Vim9 script:

<pre>
    vim9script
    filename: typstmake.vim
    set shellpipe=2>&1\|\ sed\ 's/\\x1B\\[\\([0-9]\\+;\\)*[0-9]\\+m//g\'\ >%s
    set makeprg=typst.exe\ compile\ %:S
    set errorformat=
        \%E%trror:\ %m,
        \%C%.%#┌─\ %f:%l:%c,
        \%C%.%#│,
        \%C%.%#│%.%#,
        \%C%.%#│\ \ \ \%p^,
        \%Z
</pre>

Typst's error output includes Ansi color codes for the terminal which may appear unresolved inside the quickfix list when Dispatch.vim's '':Make'' is used.



