== PYTHON-DENO-VLANG WIN32 API Message Box == 

Windows OS provides a C API for graphical elements in its user's interface (WIN32API) which it's accessible by a programming language that supports C FFI.

=== PYTHON ===

The ''ctypes'' library included in the standard library has a dedicated namespace ''windll'' for Windows shared libraries. ''Sysinternals'' [https://learn.microsoft.com/en-us/sysinternals/downloads/listdlls Listdlls.exe] command line application can show the DLLs associated with a specific Python REPL, after importing the ctypes library the following DLLs typically will be added as successive namespaces to ''windll''.

<pre>
    > 0x00000000534c0000  0xf000    C:\Users\Seve\AppData\Local\Programs\Python\Python39\python3.DLL
    > 0x000000003c540000  0x21000   C:\Users\Seve\AppData\Local\Programs\Python\Python39\DLLs\_ctypes.pyd
    > 0x000000005ba20000  0x12a000  C:\Windows\System32\ole32.dll
    > 0x000000005a570000  0x355000  C:\Windows\System32\combase.dll
    > 0x00000000532b0000  0xb000    C:\Users\Seve\AppData\Local\Programs\Python\Python39\DLLs\libffi-7.dll
    > 0x000000005b970000  0x2b000   C:\Windows\System32\GDI32.dll
    > 0x0000000059a90000  0x22000   C:\Windows\System32\win32u.dll
    > 0x0000000059d10000  0x10f000  C:\Windows\System32\gdi32full.dll
    > 0x00000000599f0000  0x9d000   C:\Windows\System32\msvcp_win.dll
    > 0x000000005b7c0000  0x1a1000  C:\Windows\System32\USER32.dll
    > 0x0000000059eb0000  0xcd000   C:\Windows\System32\OLEAUT32.dll
    > 0x000000005ace0000  0x32000   C:\Windows\System32\IMM32.DLL
</pre>

The ''USER32.dll'', as per the example below, can be called for displaying a simple message box. The function UNICODE version is exported with a W appended to the name, while the ANSI version terminates with an ''A''.

<pre>

    #!/usr/bin/env python
    from ctypes import *

    MB_OK = 0
    result = windll.user32.MessageBoxW(0, "Message to be displayed", "Dialog box title", MB_OK)
    print(f"{result}")

</pre>

=== DENO ===

[https://tessarinseve.pythonanywhere.com/nws/2022-10-18.wiki.html DENO] has a wrapper for common Win32 API calls. Specifically for a message box, the same result previously obtained with Python can be produced with the example code below.
<pre>

    import * as win32 from "https://deno.land/x/win32/mod.ts";

    const msgboxID = win32.MessageBox(
      null,
      "Message to be displayed",
      "Dialog box title",
      win32.MB_OK | win32.MB_ICONASTERISK,
    );

    console.log(msgboxID)

</pre>

It can be run locally or from a URL as follows: 
    deno.exe run --allow-ffi --unstable https://tessarinseve.pythonanywhere.com/staticdeno/MessageBox.ts

=== VLANG ===

V programming language (VLANG) can call Windows's native modal dialog box via the standard WIN32 API. The message box dialog can be accessed with the C prefix after it has been redeclared in V. 
<pre>
    #include <Windows.h>
    // filename: vmb.v
    fn C.MessageBox(int , &u16  , &u16, int) int

    fn main() 
    {
       title := 'Dialog box title'
       msg := 'Message to be displayed'
       C.MessageBox(0, msg.to_wide(), title.to_wide(), C.MB_OK)
    }

</pre>

In this case, the [https://learn.microsoft.com/en-us/windows/win32/api/winuser/nf-winuser-messagebox type of message box (MB_OK)] contains a single ''OK'' push button, as shown in this video.
The built-in string method '' to_wide'' returns a pointer to a UTF-16 string required by the Windows API that expects LPWSTR or wchar_t* parameters. 

YouTubeVideo("jbbg9qi0tps")
