
== GNU Octave ==

GNU Octave and Matlab are similar numerical computing environments. The former free while the latter commercial and has several additional features. Often, when you don't have Matlab at hand, GNU Octave can be a precious substitute.

To install GNU Octave I downloaded the latest ZIP file (at the time of writing octave-6.4.0-w64.zip) from https://www.gnu.org/software/octave/download, and extracted onto a directory that has no spaces or non-ASCII characters in its path. Manually, I created the following  ''.bat'' script inside the Octave's root directory:

    @echo off
    start octave.vbs --no-gui

together with its desktop shortcut.

Image("octave_desktopshortcut.PNG")

Prior to running Octave the first time, I executed the ''post-install.bat'' file. A quick glance at the file structure shows that the Windows version of GNU Octave has been compiled with the MINGW-64W toolchain.


=== VIM as default editor ===

It's clear that I want to have VIM (gVIM) as my default editor. On ''%USERPROFILE%'' I created the ''.octaverc'' which includes the following lines:

    EDITOR ("vim")
    edit ("mode", "sync");

    cd %USERPROFILE%
    cd Octave_Workspace

It defines VIM as the default text editor and sets the default working directory. I also created a [https://gitlab.com/Sevepy/octave.vim VIM plugin] that includes the syntax highlighting file for [https://www.vim.org/scripts/script.php?script_id=3600 GNU octave language].

=== VIM Snippets ===

The VIM extension defines a filetype (Octave) for buffers that display .m or .oct files. As shown in the previous post, it's possible to have a dedicated snippets file.

Right now my snippets file contains mostly code-snippets for creating dynamically documented scripts. 
Let's for instance use the following script as an example.
    <pre>
    %% Roots of Quadratic Equations 

    ezplot(@(x,y) 2*x.^2 +x.*4-y.- 4)
    title "2 x^2 + 4 x - 4"
    hold on
    grid on
    rts = roots([2,4,-4]);

    % Display intersections
    plot(rts,zeros(size(rts)),'*',"color","g",'MarkerSize',25);

    % x-axis 
    line(xlim,[0 0], 'linestyle', '-',"color","r")

    ## Ref.
    # \<octave:publish The PUBLISH function\>
    </pre>

As shown in the video below, the function ''publish'' can be invoked from the REPL and it creates an HTML page with a table of contents and graphs.

YouTubeVideo("sWEbfL6PELY")

=== Customise publish function ===

The filename without suffix is displayed as the header of the page, often this can be undesirable.
It's possible to modify the layout of the html page by editing the following script: 
	
    /mingw64/share/octave/6.4.0/m/miscelalaneous/private/__publish_html_output__.m 

I, for instance, have replaced the Mathjax CDN with [https://tessarinseve.pythonanywhere.com/nws/2021-06-16.wiki.html one hosted on my server].

Keep in mind, this arrangement will be lost when you update Octave with a newer version.

=== Gnuplot ===

Octave for Windows contains also several standalone applications like ''gnuplot'' and ''notepad++''.
Gnuplot was the original graphic toolkit, now replaced with a more modern one, but it's still available.
The following command replaces the Octave REPL with Gnuplot REPL:
	exec gnuplot.exe

