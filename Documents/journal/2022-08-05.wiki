
== Yes, Vim Can Call DLLs ==

Windows shared libraries are called Dynamic-link libraries or ''DLLs'', where .dll is used as their usual file extension. VIM amazingly can call a function inside a dll library (see :h libcall).

When you run a shell command from the command mode, Vim gets temporarily suspended and the command is then run from the underlying shell (bash or command prompt). When the process has been completed, Vim gets resumed in foreground. 

Alternatively, it's also possible to open a terminal buffer with 
    :term 
and directly interact with the command line.

This note instead describes how to create a simple dll with [https://vlang.io V] and then call the function inside the dll from VIM command mode.

=== LIBCALL ===

As an example, I have written the following simple file that 
opens Windows ''File Explorer''. 
    
    <pre>
    // filename: vimdll.v
    module vimdll
    import os

    [export: 'FileExplorer']
    fn test_dll(s string) &string {
        os.system("start .") 
        done := "done"
        return &done
    }
    </pre>

The dll can be obtained when the previous file is compiled with the option ''shared'' and ''prod''.
   $ v.exe -shared -prod vimdll.v

This creates the file vimdll.dll in the same directory.
To run the function test_dll from VIM, as shown in the video below, I have to type:
    :call libcall("C:\\path\to\\dll","FileExplorer","")

YouTubeVideo("ZmC33nAnNQA")
