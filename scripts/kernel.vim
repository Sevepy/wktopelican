vim9script
var base = expand('<sfile>:p:h')

def Watch_stdin() 
  var input = base .. '/' .. getpid() .. '.input'
  while 1
    sleep 500m
    if !filereadable(input)
      continue
    endif

    var vimkernel_display_data = v:none
    var stdout = ""
    var stderr = ''
    try
      redir => stdout
      exe 'source' input
    catch
      stderr = v:exception
    finally
      redir END
      if &filetype == 'help'
        var l1 = line('.')
        silent normal! j
        silent var l2 = search('\*[^*]\+\*$', 'W')
        var lines = getline(l1, getline('$'))
        stdout = join(lines, "\n")
        close
      endif
      echo stdout
      if !empty(stdout) && stdout[0] == "\n"
         stdout = stdout[1 : ]
      endif
      call delete(input)
      var temp = base .. '/' .. getpid() .. '.temp'
      var res = {
      \  'stdout': stdout,
      \  'stderr': stderr
      \}
      if !empty(vimkernel_display_data)
        res['data'] = string(vimkernel_display_data)
      endif
      call writefile([json_encode(res)], temp)
        var output = base .. '/' .. getpid() .. '.output'
      call rename(temp, output)
    endtry
  endwhile
enddef

set encoding=utf-8
var rpt = "set rtp+=" .. base
exe rpt
# call timer_start(500, function('s:watch_stdin'), {'repeat': -1})
call Watch_stdin()
