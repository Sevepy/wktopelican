PY?=python3
PELICAN?=pelican
PELICANOPTS= 

BASEDIR=$(CURDIR)
INPUTDIR=$(BASEDIR)/content
OUTPUTDIR=$(BASEDIR)/public
CONFFILE=$(BASEDIR)/pelicanconf.py
PUBLISHCONF=$(BASEDIR)/publishconf.py
WIKINEWFILE:=$(shell git ls-files -o  --exclude-standard --full-name)


all: $(OUTPUTDIR)/%.wiki.html $(OUTPUTDIR)/rss.xml $(BASEDIR)/tags/*.pickle $(OUTPUTDIR)/sitemap.xml

$(BASEDIR)/tags/%.pickle:
	./tagger.py

$(OUTPUTDIR)/%.wiki.html.wiki.html: $(BASEDIR)/tags/*.pickle
	python buildheaders.py  2> pelican.log
	sh postheader.sh  2>> pelican.log
	$(PELICAN) $(INPUTDIR) -o $(OUTPUTDIR) -s $(CONFFILE) $(PELICANOPTS)  &>>  $(BASEDIR)/pelican.log

$(OUTPUTDIR)/%.wiki.html:$(OUTPUTDIR)/*.wiki.html.wiki.html
	./postprocessing.sh
	./htmlbookmarks.py

$(OUTPUTDIR)/rss.xml $(OUTPUTDIR)/atom.xml:
	./Afeedgen.py

$(OUTPUTDIR)/sitemap.xml:
	./sitemap.sh

dict:~/AppData/Local/Programs/WinDiction/.diction_tmp 
	./diction_tmp.exe -f ~/AppData/Local/Programs/WinDiction/.diction_tmp > ./public/diction.html


clean: 
	./clean.sh


robots:robots.txt
	cp ./robots.txt ./public/robots.txt

git:
	@echo $(WIKINEWFILE)

nb:
	jupyter notebook --notebook-dir $(BASEDIR)/Documents/IPYNB


serve:
ifdef PORT
	$(PELICAN) -l $(INPUTDIR) -o $(OUTPUTDIR) -s $(CONFFILE) $(PELICANOPTS) -p $(PORT)
else
	$(PELICAN) -l $(INPUTDIR) -o $(OUTPUTDIR) -s $(CONFFILE) $(PELICANOPTS)
endif

@PHONY: clean serve git nb


