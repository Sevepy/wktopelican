#!/usr/bin/env python
import time
from subprocess import check_output
import datetime
from feedgen.feed import FeedGenerator
from wikitextparser import remove_markup, parse
from os import walk, path
import pathlib
import pdb
import pickle
import re
import pytz
if __name__ == "__main__":

    fg = FeedGenerator()

    fg.id(f'https://tessarinseve.pythonanywhere.com/nws/')
    fg.title("@Sevepy's website")
    fg.description("Seve's journal blog.")
    fg.link( href=f'https://tessarinseve.pythonanywhere.com/nws/', rel='alternate' )

    month_ago = (datetime.date.today()-datetime.timedelta(365/12))
    for (root, dirnames, filenames) in walk("./Documents/journal"):
        for file in filenames:
            if ".wiki" in file and ".hdr" not in file:

                try:
                    match = re.search(r'\d{4}-\d{2}-\d{2}', file)
                    created = datetime.datetime.strptime(match.group(), '%Y-%m-%d').astimezone(pytz.UTC)
                except AttributeError:
                    pass
                modified = datetime.datetime.strptime(time.ctime((path.getctime(f"{root}/{file}")))
                        ,'%a %b %d %H:%M:%S %Y')
                if (created.date()) > month_ago:
                    print(f" pickle : {file}")
                    try:
                        picklefile = file.replace(".wiki",".wiki.pickle")
                        with open(f"./tags/{picklefile}","rb") as tag:
                            kw = tag.read()
                        ## desc = "Keywords:"+", ".join(pickle.loads(kw))
                        print(f"{file}.html")
                        cl = ["powershell","Get-Content", f"public/{file}.html","-Head","4"]
                        link = f"https://tessarinseve.pythonanywhere.com/nws/{file}.html"
                        journal_date = re.sub('.*(\d\d\d\d-\d\d-\d\d)[.]wiki.html$', r'\1', link)
                        # header = check_output(cl,shell=False)
                        # desc = header.decode()
                        desc = ""
                        # desc += f'{journal_date} '

                        desc += """
                            For comments and/or feedback use the following link: 
                            https://tessarinseve.pythonanywhere.com/shared/request-form.html """
                        ##print( remove_markup(header))
                    except FileNotFoundError:
                        desc += "Short description not available."

                    desc += ""
                    print(f"Modified:{modified.date()}")
                    fn = fg.add_entry()
                    print(f"{root}/{file}")
                    fn.title(file.replace(".wiki",""))
                    print(f"file:{created}")
                    fn.published(created)
                    fn.author( {'name':'@seve_py'} )
                    link = file.replace(".wiki",".wiki.html")
                    fn.description(desc)
# https://validator.w3.org/feed/check.cgi?url=https%3A%2F%2Ftessarinseve.pythonanywhere.com%2Fnws%2Fatom.xml
                    fn.id(f'https://tessarinseve.pythonanywhere.com/nws/{link}')
                    # full  URL
                    fn.link( href=f'https://tessarinseve.pythonanywhere.com/nws/{link}'
                            , rel='alternate' )
    fg.logo("https://tessarinseve.pythonanywhere.com/staticweb/twittwecardlogo.png")
                    #fn.subtitle('desc')
    #pdb.set_trace()
    fg.pubDate(datetime.datetime.now(tz=pytz.utc))
    fg.rss_str(pretty=True)
    fg.atom_str(pretty=True)
    fg.rss_file('./public/rss.xml')
    fg.atom_file('./public/atom.xml')
