#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals
from datetime import datetime
# from pelican_jupyter import markup as nb_markup
AUTHOR = '@Seve_py'
SITENAME = 'SeveTech'
SITEURL = ''
COPYRIGHT_YEAR = datetime.now().year
REPOURL = 'https://gitlab.com/Sevepy/wktopelican'

MATHJAX_DOMAIN='https://tessarinseve.pythonanywhere.com/MathJax'
MARKUP = ('md','html','ipynb')
PATH = 'content'
THEME='theme'

TIMEZONE = 'Europe/Dublin'

DEFAULT_LANG = 'en'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# # Social widget social channel, url, tooltip or title
SOCIAL = (
 ('twitter', 'https://twitter.com/Seve_py', 'Follow me'),
 ("gitlab", "https://gitlab.com/users/Sevepy/projects", "My Gitlab repos"),
 ("youtube","http://youtube.com/@sevetech", "My YouTube Channel"),
 ("pinterest", "http://www.pinterest.com/sevetech", "Pinterest"),
 )


# twitter username
TWITTER_USERNAME= "@Seve_py"


IGNORE_FILES = [".ipynb_checkpoints"]

DEFAULT_PAGINATION = 6
SUMMARY_MAX_LENGTH= 20 # words

## Dir with .png and .svg files defined in the rwe plugin
FIGURES_DIR = 'Figures'
VIDEOS_DIR = 'Videos'
TEMU_DIR = 'TEMU'

## Extra in content/extra
STATIC_PATHS = ['extra/robots.txt']

EXTRA_PATH_METADATA = {
    'extra/robots.txt': {'path': './robots.txt'}
}

# Site map plugin
#
SITEMAP = {
    'format': 'xml',
    "exclude": ["tag/","author/","category/","index\d"],
    'priorities': {
        'articles': 0.6,
        'indexes': 0.5,
        'pages': 0.4
    },
    'changefreqs': {
        'articles': 'monthly',
        'indexes': 'monthly',
        'pages': 'monthly'
    }
}

# Twitter card
CARDURL = "https://tessarinseve.pythonanywhere.com/twittercard"
# blocked by robots https://tessarinseve.pythonanywhere.com/staticweb/twittwecardlogo.png"
# Uncomment following line if you want document-relative URLs when developing
# pip install pelican_jupyter 
RELATIVE_URLS = True
PLUGIN_PATHS = ['./plugins']
PLUGINS = ['pelican-embed-tweet.embed_tweet',
	'ipynb.markup', 
	'sitemap',
	'pelican_bootstrap_rwe',
    'minify',
    'tipue_search',
	]
## tipue_search clone git tree moved plugin dir to site\plugins
DIRECT_TEMPLATES = ['index', 'tags', 'categories', 'authors', 'archives','search']
# custom Jinja2 filter
def get_by_slug(objs, slug):
    for obj in objs:
        if obj.slug == slug:
            return obj


# custom Jinja2 filter - to use with not iterable like article
def remove_by_slug(obj, slug):
    if obj.slug == slug:
            return False

def datetonumber(datetime, format='%a-%d-%m-%Y'):
    return datetime.date().strftime(format)

def fixtitle(title):
    """
    category title = slug with extension

    """
    return title.replace("_"," ").replace(".wiki.html","")
def fixurl(url):
    """
    fix internal url with vim.wiki standard

    """
    return url.replace(".wiki.html.wiki.html",".wiki.html")
JINJA_FILTERS = {
    "fixtitle": fixtitle,
    "fixurl": fixurl,
    "remove_by_slug": remove_by_slug,
    "get_by_slug": get_by_slug,
    'datetonumber':datetonumber,
}
